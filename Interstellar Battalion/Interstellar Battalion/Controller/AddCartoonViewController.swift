//
//  AddCartoonViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 13/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class AddCartoonViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var cartoonImage: UIImageView!
    @IBOutlet weak var cartoonNameTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var selectImageButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    var cartoonName : String?
    var image : String?
    var descriptionText : String?
    var id : Int?
    
    var isEdit: Bool = false
    var imageStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        _ = self.textFieldShouldReturn(cartoonNameTextField)
        
        if cartoonName != nil && image != nil && descriptionText != nil {
            setupForEdit()
            cartoonNameTextField.isEnabled = false
            descriptionTextView.isEditable = false
            selectImageButton.isEnabled = false
            saveButton.isEnabled = false
        }
    }
    
    func setupForEdit() {
        guard let cartoonNameForTitle = cartoonName else {
            self.title = VIEW_CONTROLLER_TITLE_FOR_EDIT
            return
        }
        self.title = createTitleForCartoon(cartoonNameForTitle: cartoonNameForTitle)
        
        guard let cartoonNameForEdit = cartoonName else {
            cartoonNameTextField.placeholder = CARTOON_NAME_TEXTFIELD_PLACEHOLDER
            return
        }
        cartoonNameTextField.text = cartoonNameForEdit
        
        guard let cartoonImageForEdit = image else {
            cartoonImage.image = UIImage(named: IMAGE_NAME_DEFAULT_CARTOON)
            return
        }
        if let dataDecoded : Data = Data(base64Encoded: cartoonImageForEdit, options: .ignoreUnknownCharacters) {
            cartoonImage.image = UIImage(data: dataDecoded)
        }
        
        if descriptionText != nil {
            descriptionTextView.text = descriptionText
        } else {
            descriptionTextView.text = CARTOON_DESCRIPTION_TEXT_NOT_FOUND
        }
    }
    
    @IBAction func selectImageButtonPressed(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: ALERT_CONTROLLER_TITLE, message: ALERT_CONTROLLER_MESSAGE, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_CAMERA_TITLE, style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                self.showAlert(title: CAMERA_FAILED_TITLE, message: CAMERA_FAILED_MESSAGE)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_GALLERY_TITLE, style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_CANCEL_TITLE, style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    @IBAction func editCartoons(_ sender: Any) {
        cartoonNameTextField.isEnabled = true
        descriptionTextView.isEditable = true
        selectImageButton.isEnabled = true
        saveButton.isEnabled = true
        isEdit = true
    }
    
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if isEdit {
            self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPDATING)
            CartoonService.instance.editCartoon(id: id!, cartoonName: cartoonNameTextField.text!, description: descriptionTextView.text, image: imageStr!) {[weak self] (success) in
                
                self?.hideLoadIndicator()
                if success {
                    self?.showAlertWithOk(title: ALERT_FOR_CARTOON_SAVED_TITLE, message: ALERT_FOR_CARTOON_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                        self?.performSegue(withIdentifier: UNWIND_TO_CARTOON, sender: nil)
                    })
                }
            }
        } else {
            if imageStr == "" && cartoonNameTextField.text == "" && descriptionTextView.text == "" {
                self.showAlert(title: ALERT_FOR_CARTOON_SAVING_FAILED_TITLE, message: ALERT_FOR_CARTOON_SAVING_FAILED_MESSAGE)
            } else {
                self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPLOADING)
                CartoonService.instance.createCartoon(cartoonName: cartoonNameTextField.text!, description: descriptionTextView.text, image: imageStr!) {[weak self] (success) in
                    
                    self?.hideLoadIndicator()
                    if success {
                        self?.showAlertWithOk(title: ALERT_FOR_CARTOON_SAVED_TITLE, message: ALERT_FOR_CARTOON_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                            self?.performSegue(withIdentifier: UNWIND_TO_CARTOON, sender: nil)
                        })
                    }
                }
            }
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        cartoonImage.image = image
        picker.dismiss(animated: true, completion: nil)
        
        let imageData:NSData = UIImageJPEGRepresentation(image, 0.50)! as NSData
        imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
