//
//  PhotoCellForToys.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 15/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation
import Alamofire

class PhotoCellForToys: UICollectionViewCell {
    @IBOutlet weak var photoImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
    
    var record:ToyDataModel!{
        didSet{
            
            if let dataDecoded : Data = Data(base64Encoded: record.image, options: .ignoreUnknownCharacters) {
                photoImg.image = UIImage(data: dataDecoded)
            }
        }
    }
    
    func setUpView() {
        //self.layer.backgroundColor = #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImg.image = nil
    }
    
}
