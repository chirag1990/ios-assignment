//
//  ToyService.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 15/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class ToyService {
    
    static let instance = ToyService()
    
    fileprivate var toyRecords = [ToyDataModel]()
    
    var dataSource : [ToyDataModel] {
        get {
            return toyRecords
        }
    }
    
    func getToys(completion: @escaping CompletionHandler) {
        Alamofire.request(URL_GET_TOYS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            
            if response.result.error == nil {
                guard let response = response.result.value as? [String:Any] else {return}
                
                guard let data = response["data"] as? [[String:Any]] else {
                    completion(false)
                    return
                }
                self.toyRecords.removeAll()
                for record in data {
                    let toyRecordData = ToyDataModel(dict: record)
                    self.toyRecords.append(toyRecordData)
                }
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func addToys(toyName: String, description: String, image: String,completion: @escaping CompletionHandler) {
        let body: [String: Any] = [
            "toyName": toyName,
            "description": description,
            "image" : image
        ]
        
        Alamofire.request(URL_ADD_TOYS, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func editToys(id : Int, toyName: String, description: String, image: String, completion: @escaping CompletionHandler) {
        
        let body: [String: Any] = [
            "toyName": toyName,
            "description": description,
            "image" : image
        ]
        
        Alamofire.request(editToysUrl(id: id), method: .put, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
}
