//
//  CartoonCollectionViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 13/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class CartoonCollectionViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var dataSource = CartoonService.instance.dataSource
    
    var cartoonName : String?
    var cartoonImage : String?
    var cartoonDesc : String?
    var cartoonId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        setup()
    }
    
    func setup() {
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_LOADING)
        CartoonService.instance.getCartoons() {[weak self] (success) in
            if success {
                self?.hideLoadIndicator()
                self?.reloadPage()
            }
        }
    }
    
    fileprivate func reloadPage(){
        self.dataSource = CartoonService.instance.dataSource
        self.collectionView.reloadData()
    }
    
    @IBAction func addCartoonButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_ADD_CARTOONS, sender: nil)
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        self.setup()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == EDIT_CARTOON_SEGUE_IDENTIFIER {
            let destinationNavigationController = segue.destination as! UINavigationController
            let vc = destinationNavigationController.topViewController as! AddCartoonViewController
            vc.image = cartoonImage
            vc.cartoonName = cartoonName
            vc.descriptionText = cartoonDesc
            vc.id = cartoonId
        }
    }
    
}

extension CartoonCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_LOADING)
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTIONVIEW_CELL_IDENTIFIER, for: indexPath) as? PhotoCell {
            cell.record = self.dataSource[indexPath.row]
            self.hideLoadIndicator()
            return cell
        }
        return PhotoCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cartoonImage = self.dataSource[indexPath.row].image
        cartoonName = self.dataSource[indexPath.row].cartoon_name
        cartoonDesc = self.dataSource[indexPath.row].description
        cartoonId = self.dataSource[indexPath.row].id
        performSegue(withIdentifier: EDIT_CARTOON_SEGUE_IDENTIFIER, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // number of columns
        let numOfColumns : CGFloat = 2
        // space between cells
        let spaceBetweenCells: CGFloat = 10
        // padding around cells
        let padding : CGFloat = 40
        // calculation to display 2 column for every device sice
        let cellDimension = ((collectionView.bounds.width - padding) - (numOfColumns) * spaceBetweenCells) / numOfColumns
        
        return CGSize(width: cellDimension,  height: cellDimension)
    }
    
}
