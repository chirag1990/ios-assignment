//
//  AddCastProfileViewController.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 08/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

protocol AddCastProfileViewControllerDelegate:class {
    func refreshCastData()
}

class AddCastProfileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate
{
    //MARK:- IBOutlet
    @IBOutlet weak var profileImage: CircleImage!
    @IBOutlet weak var castDescription: UITextView!
    @IBOutlet weak var nameTextField: UITextField!
    
    var image : String?
    var name : String?
    var desc : String?
    var id : Int?
    var castId: Int?
    var isEdit :Bool = false
    
    var imageStr : String?
    var delegate:AddCastProfileViewControllerDelegate?
    
    //MARK:-
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        if image != nil && name != nil && desc != nil {
            isEdit = true
            setUpForEdit()
        }
    }
    
    func setUpForEdit() {
        if let dataDecoded : Data = Data(base64Encoded: image!, options: .ignoreUnknownCharacters) {
            profileImage.image = UIImage(data: dataDecoded)
        }
        nameTextField.text = name
        castDescription.text = desc
    }
    
    //MARK:-IBAction
    @IBAction func uploadImagePressed(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: ALERT_CONTROLLER_TITLE, message: ALERT_CONTROLLER_MESSAGE, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_CAMERA_TITLE, style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                self.showAlert(title: CAMERA_FAILED_TITLE, message: CAMERA_FAILED_MESSAGE)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_GALLERY_TITLE, style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_CANCEL_TITLE, style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any)
    {
        if isEdit {
            self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPDATING)
            guard let castId = id, id != 0 else {return}
            imageStr = image
            CastService.instance.editCast(id: castId, image: imageStr!, name: nameTextField.text!, description: castDescription.text) {[weak self] (success) in
                self?.hideLoadIndicator()
                if success {
                    self?.showAlertWithOk(title: ALERT_FOR_UPDATE_COMPLETED_TITLE, message: ALERT_FOR_UPDATE_COMPLETED_MESSAGE, completionHandler: {[weak self] (success) in
                        self?.delegate?.refreshCastData()
                        self?.navigationController?.popToRootViewController(animated: true)
                        //self?.performSegue(withIdentifier: UNWIND_TO_CAST_TABLE_VIEW, sender: self)
                    })
                }
            }
        } else {
            if imageStr == "" && castDescription.text == "" && nameTextField.text == "" {
                self.showAlert(title: ALERT_FOR_CAST_SAVING_FAILED_TITLE, message: ALERT_FOR_CAST_SAVING_FAILED_MESSAGE)
            } else {
                self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPLOADING)
                CastService.instance.saveCast(image: imageStr!, name: nameTextField.text!, description: castDescription.text) {[weak self] (success) in
                    
                    self?.hideLoadIndicator()
                    if success {
                        self?.showAlertWithOk(title: ALERT_FOR_CAST_SAVED_TITLE, message: ALERT_FOR_CAST_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                            self?.delegate?.refreshCastData()
                            self?.navigationController?.popViewController(animated: true)
                        })
                    }
                }
            }
        }
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileImage.image = image
        picker.dismiss(animated: true, completion: nil)
        
        let imageData:NSData = UIImageJPEGRepresentation(image, 0.50)! as NSData
        imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_BACK_CAST_PROFILE, sender: self)
    }
    
    
}

extension AddCastProfileViewController
{
    struct Constant {
        static let identifier = "AddCastProfileIdentifier"
    }
}


