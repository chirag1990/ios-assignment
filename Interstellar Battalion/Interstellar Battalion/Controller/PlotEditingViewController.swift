//
//  PlotEditingViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 07/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class PlotEditingViewController: UIViewController {

    @IBOutlet weak var plotTextView: UITextView!
    @IBOutlet weak var urlTextField: UITextField!
    var success: Bool?
    var plot:[Plot]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.leftItemsSupplementBackButton = true;
        urlTextField.delegate = self
        setupUI()
        self.hideKeyboardWhenTappedAround()
        _ = self.textFieldShouldReturn(urlTextField)
    }

    func setupUI() {
        plot = CoreDataHandler.fetchObject()!
        for plotText in plot! {
            plotTextView.text = plotText.plot
        }
    }

    @IBAction func SaveButtonPressed(_ sender: Any) {
        
        plot = CoreDataHandler.fetchObject()!
        
        if plot?.count == 0 {
            success = CoreDataHandler.saveObject(plot: plotTextView.text!, url: urlTextField.text!)
        } else {
            let oldPlot = plot![0].plot
            success = CoreDataHandler.updateObject(plot: plotTextView.text!, url: urlTextField.text!, oldPlot: oldPlot!)
        }
        
        if success! {
            self.showAlertWithOk(title: PLOT_CREATED_TITLE, message: PLOT_CREATED_MESSAGE, completionHandler: {[weak self] (success) in
                self?.performSegue(withIdentifier: UNWIND_TO_HOME, sender: self)
            })
        } else {
            self.showAlert(title: PLOT_CREATED_FAILED_TITLE, message: PLOT_CREATED_FAILED_MESSAGE)
        }
    }
}
