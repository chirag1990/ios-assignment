//
//  AddCastCareerViewController.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 11/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class AddCastCareerViewController: UIViewController {

    @IBOutlet weak var enterMovieTextField: UITextField!
    @IBOutlet weak var releaseDateTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var barButtonItem: UIBarButtonItem!
    
    var id : Int?
    var movieName : String?
    var releaseDate : String?
    var shortDesc : String?
    var careerId : Int?
    
    var isEdit:Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        _ = textFieldShouldReturn(enterMovieTextField)
        setupUI()
    }
    
    func setupUI() {
        if movieName != nil {
            self.title = CAST_EDIT_CAREER_TITLE
            enterMovieTextField.text = movieName
            releaseDateTextField.text = releaseDate
            descriptionTextView.text = shortDesc
            enterMovieTextField.isEnabled = false
            releaseDateTextField.isEnabled = false
            descriptionTextView.isEditable = false
            self.title = "Edit Cast Career"
        }
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        if enterMovieTextField.text != "" && releaseDateTextField.text != "" && descriptionTextView.text != "" {
            self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPLOADING)
            if isEdit {
                CastCareerService.instance.editCareer(id: id!, career_id: careerId!, releaseDate: releaseDateTextField.text!, name: enterMovieTextField.text!, description: descriptionTextView.text, completion: { (success) in
                    if success {
                        self.hideLoadIndicator()
                        self.showAlertWithOk(title: ALERT_FOR_CAST_SAVED_TITLE, message: ALERT_FOR_CAST_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                            
                            self?.performSegue(withIdentifier: SEGUE_BACK_CAST_PROFILE, sender: self)
                        })
                    }
                })
            } else {
                CastCareerService.instance.createCastCareer(cast_id: id!, movieName: enterMovieTextField.text!, releaseDate: releaseDateTextField.text!, shortDescription: descriptionTextView.text, completion: { (success) in
                    if success {
                        self.hideLoadIndicator()
                        self.showAlertWithOk(title: ALERT_FOR_CAST_SAVED_TITLE, message: ALERT_FOR_CAST_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                            
                            self?.performSegue(withIdentifier: SEGUE_BACK_CAST_PROFILE, sender: self)
                        })
                    }
                })
            }
        }
    }
    
    

    @IBAction func editButtonPressed(_ sender: Any) {
        isEdit = true
        enterMovieTextField.isEnabled = true
        releaseDateTextField.isEnabled = true
        descriptionTextView.isEditable = true
    }
    
    @IBAction func textFieldEditForDatePicker(_ sender: UITextField) {
        setupDatePicker()
        let datePickerView : UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    
    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        releaseDateTextField.text = dateFormatter.string(from: sender.date)
 
    }
    
    func setupDatePicker(){
        let toolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        toolBar.barStyle = UIBarStyle.blackTranslucent
        toolBar.tintColor = UIColor.white
        toolBar.backgroundColor = #colorLiteral(red: 0.9783681035, green: 0.3710477352, blue: 0.3826945424, alpha: 1)
        let todayBtn = UIBarButtonItem(title: BAR_BUTTON_ITEM_TITLE, style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.tappedToolBarBtn))
        let okBarBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(self.donePressed))
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: self, action: nil)
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width / 3, height: self.view.frame.size.height))
        label.font = UIFont(name: LABEL_FONT_HELVETICA, size: 12)
        label.backgroundColor = UIColor.clear
        label.textColor = UIColor.white
        label.text = LABEL_FOR_SELECT_DATE
        label.textAlignment = NSTextAlignment.center
        let textBtn = UIBarButtonItem(customView: label)
        toolBar.setItems([todayBtn,flexSpace,textBtn,flexSpace,okBarBtn], animated: true)
        releaseDateTextField.inputAccessoryView = toolBar
    }
    
    @objc func donePressed(sender: UIBarButtonItem) {
        releaseDateTextField.resignFirstResponder()
    }
    
    @objc func tappedToolBarBtn(sender: UIBarButtonItem) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "yyyy-MM-dd"
        releaseDateTextField.text = dateFormatter.string(for: Date())
        
        releaseDateTextField.resignFirstResponder()
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    // MARK: - Navigation

    @IBAction func backButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: SEGUE_BACK_CAST_PROFILE, sender: self)
    }
}

