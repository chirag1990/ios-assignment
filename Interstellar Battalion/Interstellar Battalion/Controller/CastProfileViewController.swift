//
//  CastProfileViewController.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 08/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

protocol CastProfileViewControllerDelegate {
    func refreshData()
}

class CastProfileViewController: UIViewController
{

    //MARK:- IBoutelts
    @IBOutlet fileprivate weak var profileImageCircle: UIImageView!
    @IBOutlet fileprivate weak var castName: UILabel!
    @IBOutlet fileprivate weak var castDesc: UITextView!
    @IBOutlet fileprivate weak var careerTableView: UITableView!
    @IBOutlet fileprivate weak var segmentSwitch: UISegmentedControl!
    
    //MARK:- Private vars
    fileprivate var dataSource = CastCareerService.instance.dataSource
    
    //MARK:- Public vars for cast screen
    var image : String?
    var name : String?
    var desc : String?
    var id : Int?
    var castId: Int?
    
    var movieName : String?
    var releaseDate : String?
    var shortDesc : String?
    var careerId : Int?
    
    var delegate:CastProfileViewControllerDelegate?
    
    //MARK:- View Life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
        initialSettig()
        self.hideKeyboardWhenTappedAround() 
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Private Methods
    fileprivate func setUpUI() {
        if let dataDecoded : Data = Data(base64Encoded: image!, options: .ignoreUnknownCharacters) {
            profileImageCircle.image = UIImage(data: dataDecoded)
        }
        castName.text = name!
        castDesc.text = desc!
        castId = id!
        careerTableView.isHidden = true
    }
    
    fileprivate func initialSettig()
    {
        careerTableView.delegate = self
        careerTableView.dataSource = self
        
        CastCareerCell.registerNib(tableView: careerTableView)
        if segmentSwitch.selectedSegmentIndex == 1 {
            self.loadData()
        }
    }
    
    fileprivate func loadData(){
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_LOADING)
        CastCareerService.instance.getCastCareer(cast_id: id!) {[weak self] (success) in
            if success {
                self?.hideLoadIndicator()
                self?.reloadPage()
            }
        }
    }
    
    fileprivate func reloadPage(){
        self.dataSource = CastCareerService.instance.dataSource
        self.careerTableView.reloadData()
    }
    
    //MARK:- IBAction
    @IBAction func editButtonPressed(_ sender: Any) {
        if segmentSwitch.selectedSegmentIndex == 0 {
            self.performSegue(withIdentifier: SEGUE_TO_CAST_PROFILE, sender: nil)
        } else {
            self.performSegue(withIdentifier: SEGUE_TO_CAST_CAREER, sender: nil)
        }
    }
    
    @IBAction func segmentSwitchPressed(_ sender: UISegmentedControl)
    {
        switch segmentSwitch.selectedSegmentIndex {
        case 1:
            careerTableView.isHidden = false
            castDesc.isHidden = true
            self.title = CAST_CAREER_TITLE
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(editButtonPressed(_:)))
            navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.9783681035, green: 0.3710477352, blue: 0.3826945424, alpha: 1)
            loadData()
        default:
            careerTableView.isHidden = true
            castDesc.isHidden = false
            self.title = CAST_PROFILE_TITLE
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonPressed(_:)))
            navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.9783681035, green: 0.3710477352, blue: 0.3826945424, alpha: 1)
        }
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        self.initialSettig()
    }
    
    //MARK:- Other
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SEGUE_TO_CAST_PROFILE {
           // let destinationNavigationController = segue.destination as! UINavigationController
           // let vc = destinationNavigationController.topViewController as! AddCastProfileViewController
            let vc = segue.destination as! AddCastProfileViewController
            vc.delegate = self
            vc.id = id
            vc.image = image
            vc.name = name
            vc.desc = desc
        } else if segue.identifier == SEGUE_TO_CAST_CAREER {
            let destinationNavigationController = segue.destination as! UINavigationController
            let vc = destinationNavigationController.topViewController as! AddCastCareerViewController
            vc.id = id
            vc.movieName = movieName
            vc.releaseDate = releaseDate
            vc.shortDesc = shortDesc
            vc.careerId = careerId
        }
    }
}

//MARK:- Extensions

extension CastProfileViewController:AddCastProfileViewControllerDelegate
{
    func refreshCastData() {
        self.delegate?.refreshData()
    }
}
extension CastProfileViewController:UITableViewDelegate
{
    
}

extension CastProfileViewController:UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CastCareerCell.CareerCellConstant.Height
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = careerTableView.dequeueReusableCell(withIdentifier: CastCareerCell.CareerCellConstant.Identifier) as! CastCareerCell
        cell.record = dataSource[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        movieName = self.dataSource[indexPath.row].movie_name
        releaseDate = self.dataSource[indexPath.row].release_date
        shortDesc = self.dataSource[indexPath.row].short_description
        careerId = self.dataSource[indexPath.row].id
        performSegue(withIdentifier: SEGUE_TO_CAST_CAREER, sender: nil)
    }
}

