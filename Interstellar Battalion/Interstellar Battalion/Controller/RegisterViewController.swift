//
//  RegisterViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 07/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    var successRegistering = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        _ = textFieldShouldReturn(username)
        _ = self.textFieldShouldReturn(password)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func registerButton(_ sender: Any) {
       
        if username.text == "" || password.text == "" {
            self.showAlert(title: USER_LOGIN_FAILED_TITLE, message: USER_LOGIN_FAILED_MESSAGE)
        } else {
            AuthService.instance.registerUser(email: username.text ?? "", password: password.text ?? "") { (success) in
                if success {
                    AuthService.instance.loginUser(email: self.username.text ?? "", password: self.password.text ?? "", completion: { (success) in
                        if success {
                            if AuthService.instance.isLoggedIn {
                                self.performSegue(withIdentifier: SEGUE_TO_HOME_FROM_LOGIN, sender: nil)
                            }
                        }
                    })
                } else {
                    self.showAlert(title: USER_REGISTRATION_FAILED_TITLE, message: USER_REGISTRATION_FAILED_MESSAGE)
                }
            }
        }
    }
    
    @IBAction func signInButton(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_SIGN_IN, sender: nil)
    }
}
