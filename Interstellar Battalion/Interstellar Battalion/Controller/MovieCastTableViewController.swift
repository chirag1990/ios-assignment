//
//  MovieCastTableViewController.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 08/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class MovieCastTableViewController: UITableViewController
{
    @IBOutlet fileprivate var tableview: UITableView!
    fileprivate var dataSource = CastService.instance.datsSource
    var id : Int?
    var image : String?
    var name : String?
    var desc : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSettig()
        self.hideKeyboardWhenTappedAround()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    fileprivate func initialSettig(){
        tableview.delegate = self
        tableview.dataSource = self
        
        CastCell.registerNib(tableView: tableview)
        self.loadData()
    }
    
    fileprivate func pageAppearance(){
        //Any Page formataion
    }
    
    fileprivate func loadData(){
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_LOADING)
        CastService.instance.getCast {[weak self] (success) in
            if success {
                self?.hideLoadIndicator()
                self?.reloadPage()
            }
        }
    }
    
    fileprivate func reloadPage(){
        self.dataSource = CastService.instance.datsSource
        self.tableview.reloadData()
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: CastCell.CellConstant.Identifier) as! CastCell
        cell.record = dataSource[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CastCell.CellConstant.Height
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            guard let id = CastService.instance.datsSource[indexPath.row].id else {return}
            CastService.instance.deleteCast(id: id, completion: { (success) in
                if success {
                    self.dataSource.remove(at: indexPath.row)
                    tableView.deleteRows(at: [indexPath], with: .fade)
                }
            })
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        id = self.dataSource[indexPath.row].id
        image = self.dataSource[indexPath.row].image
        name = self.dataSource[indexPath.row].name
        desc = self.dataSource[indexPath.row].des
        performSegue(withIdentifier: SEGUE_TO_CAST_PROFILE, sender: nil)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == AddCastProfileViewController.Constant.identifier{
            let vc = segue.destination as! AddCastProfileViewController
           // let vc = destinationNavigationController.topViewController as! AddCastProfileViewController
            vc.delegate = self
        } else if segue.identifier == SEGUE_TO_CAST_PROFILE {
            let vc = segue.destination as! CastProfileViewController
            vc.delegate = self
            vc.id = id
            vc.image = image
            vc.name = name
            vc.desc = desc
        }
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        self.initialSettig()
    }

}

//MARK:- Extension
extension MovieCastTableViewController:AddCastProfileViewControllerDelegate
{
    func refreshCastData() {
        self.loadData()
    }
}

extension MovieCastTableViewController:CastProfileViewControllerDelegate
{
    func refreshData() {
        self.loadData()
    }
}
