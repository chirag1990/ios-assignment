//
//  ToyDataModel.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 15/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation

struct ToyDataModel {
    let id : Int!
    let toy_name : String!
    let image : String!
    let description : String!
    let updated_at: String!
    let created_at: String!
    
    init(dict:[String:Any]){
        self.id = dict["id"] as! Int
        self.toy_name = dict["toy_name"] as! String
        self.image = dict["image"] as! String
        self.description = dict["description"] as! String
        self.updated_at = dict["updated_at"] as! String
        self.created_at = dict["created_at"] as! String
    }
}
