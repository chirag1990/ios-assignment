//
//  Constants.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 08/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation


typealias CompletionHandler = (_ Success: Bool) -> ()
typealias AlertCompletionHandler = (_ okClicked:Bool) -> Void

// Service URL Constants
let BASE_URL = "http://chiragapi.code-fresh.com/v1"
let URL_REGISTER = "\(BASE_URL)/register"
let URL_LOGIN = "\(BASE_URL)/login"
let URL_ADD_CAST = "\(BASE_URL)/cast-screens"
let URL_GET_CAST = "\(BASE_URL)/cast-screens"
let URL_GET_CARTOONS = "\(BASE_URL)/cartoons"
let URL_ADD_CARTOONS = "\(BASE_URL)/cartoons"
let URL_GET_TOYS = "\(BASE_URL)/toys"
let URL_ADD_TOYS = "\(BASE_URL)/toys"

//Functions to create URLs with variable provided by user
func deleteCastProfile(id: Int) -> String {
    return "\(BASE_URL)/cast-screens/\(id)"
}

func editCastCareer(id: Int, career_id : Int) -> String {
    return "\(BASE_URL)/cast-screens/\(id)/careers/\(career_id)"
}

func editCastProfile(id: Int) -> String {
    return "\(BASE_URL)/cast-screens/\(id)"
}

func getCastCareerUrl(cast_id: Int) -> String {
    return "\(BASE_URL)/cast-screens/\(cast_id)/careers"
}

func createCastCareerUrl(cast_id: Int) -> String {
    return "\(BASE_URL)/cast-screens/\(cast_id)/careers"
}

func editCartoonUrl(id :Int) -> String {
    return "\(BASE_URL)/cartoons/\(id)"
}

func editToysUrl(id :Int) -> String {
    return "\(BASE_URL)/toys/\(id)"
}

let MOVIE_PLOT = "Interstellar Battalion is a £70 million block buster based on the invasion of Earth by an alien race (the Giganti) and its superior technology. The film is set in 2020 and begins with the aliens attacking China, Russia, and rapidly occupying most of Asia before stopping at the edge of Europe. Despite a combined resistance from earth the invasion has been rapid taking just 3 days. The reason the aliens have stopped is to offer the surviving humans a chance to surrender before a second attack would commence. The film ends at this point as there is to be a sequel where earth attempts to repel the invasion."


let MOVIE_TRAILER_URL = "https://www.youtube.com/embed/zSWdZVtXT7E"

//Login And Registration Constants
let USER_REGISTRATION_FAILED_TITLE = "User Registration Failed"
let USER_REGISTRATION_FAILED_MESSAGE = "There has been an problem registering please try again later"
let USER_LOGIN_FAILED_TITLE = "User Login Failed"
let USER_LOGIN_FAILED_MESSAGE = "There has been an problem in login in please try again later"

//Plot Editing and Plot Home Constants
let PLOT_CREATED_TITLE = "Plot Created Sucessfully"
let PLOT_CREATED_MESSAGE = "Plot has been added to core data"
let PLOT_CREATED_FAILED_TITLE = "Plot Create Failed"
let PLOT_CREATED_FAILED_MESSAGE = "Plot has not been added to core data, Please try again later"

//Add Cast Screen and cast career Constants
let ALERT_CONTROLLER_TITLE = "Photo Source"
let ALERT_CONTROLLER_MESSAGE = "Choose a photo source"
let ALERT_ACTIONSHEET_CAMERA_TITLE = "Camera"
let CAMERA_FAILED_TITLE = "Camera Not Available"
let CAMERA_FAILED_MESSAGE = "Camera is currently busy please try again later"
let ALERT_ACTIONSHEET_GALLERY_TITLE = "Photo Library"
let ALERT_ACTIONSHEET_CANCEL_TITLE = "Cancel"
let ALERT_FOR_UPDATE_COMPLETED_TITLE = "Update Completed"
let ALERT_FOR_UPDATE_COMPLETED_MESSAGE = "Cast has been updated successfully"
let ALERT_FOR_CAST_SAVING_FAILED_TITLE = "Cast Saving Failed"
let ALERT_FOR_CAST_SAVING_FAILED_MESSAGE = "Error has occurred saving cast, please try again later"
let ALERT_FOR_CAST_SAVED_TITLE = "Cast Saved"
let ALERT_FOR_CAST_SAVED_MESSAGE = "Cast has been saved successfully"
let DATE_FORMAT = "yyyy-mm-dd"

// Cartoon and Add Cartoon Screen
let ALERT_FOR_CARTOON_SAVING_FAILED_TITLE = "Cartoon Saving Failed"
let ALERT_FOR_CARTOON_SAVING_FAILED_MESSAGE = "Error has occurred saving cartoon, please try again later"
let ALERT_FOR_CARTOON_SAVED_TITLE = "Cartoon Saved"
let ALERT_FOR_CARTOON_SAVED_MESSAGE = "Cartoon, has been saved successfully"
let VIEW_CONTROLLER_TITLE_FOR_EDIT = "Edit Cartoon"
let IMAGE_NAME_DEFAULT_CARTOON = "cartoon"
let CARTOON_NAME_TEXTFIELD_PLACEHOLDER = "Enter Cartoon Name, As it wasnt found"
let CARTOON_DESCRIPTION_TEXT_NOT_FOUND = "Description Not Found"

let VIEW_CONTROLLER_TITLE_FOR_EDIT_ON_TOYS = "Edit Toys"

func createTitleForCartoon(cartoonNameForTitle : String) -> String {
    return "Edit \(cartoonNameForTitle) Cartoon"
}

func createTitleForToy(toyNameForTitle : String) -> String {
    return "Edit \(toyNameForTitle) Toy"
}

let TOY_NAME_TEXTFIELD_PLACEHOLDER = "Enter Toy Name, As it wasnt found"
let TOY_DESCRIPTION_TEXT_NOT_FOUND = "Description Not Found"
let ALERT_FOR_TOY_SAVED_TITLE = "Toy Saved"
let ALERT_FOR_TOY_SAVED_MESSAGE = "Toy, has been saved successfully"
let ALERT_FOR_TOY_SAVING_FAILED_TITLE = "Toy Saving Failed"
let ALERT_FOR_TOY_SAVING_FAILED_MESSAGE = "Error has occurred saving toy, please try again later"


// Datepicker Constants
let BAR_BUTTON_ITEM_TITLE = "Today"
let LABEL_FONT_HELVETICA = "Helvetica"
let LABEL_FOR_SELECT_DATE = "Select a release date"

// Header constant
let HEADER = [
    "Content-Type": "application/json; charset=utf-8"
]
// Auth Service Constants
let LOGGED_IN_KEY = "loggedIn"
let RETURN_MESSAGE = "Logged In"

// XIB file constants
let CAST_CELL_IDENTIFIER = "CastCell"
let CAST_CAREER_CELL_IDENTIFIER = "CastCareerCell"

// CoreData Constants
let ENTITY_NAME = "Plot"
let PLOT_KEY = "plot"
let URL_KEY = "url"

func formatStringForPredicate(oldPlot: String) -> String {
    return "plot = '\(oldPlot)'"
}

// Loading Indicator Constants
let LOAD_INDICATOR_FOR_LOADING = "Loading..."
let LOAD_INDICATOR_FOR_UPDATING = "Updating..."
let LOAD_INDICATOR_FOR_UPLOADING = "Uploading..."

// ViewController Setting Titles
let CAST_CAREER_TITLE = "Cast Career"
let CAST_PROFILE_TITLE = "Cast Profile"
let CAST_EDIT_CAREER_TITLE = "Edit Cast Career Profile"


// Segue Constants
let UNWIND_TO_HOME = "unWindToHome"
let SEGUE_TO_HOME_FROM_LOGIN = "segueToHome"
let SEGUE_TO_SIGN_IN = "signInSegue"
let SEGUE_TO_CAST_PROFILE = "CastProfile"
let SEGUE_TO_CAST_CAREER = "CastCareer"
let UNWIND_TO_CAST_TABLE_VIEW = "unwindToCastTableViewController"
let SEGUE_BACK_CAST_PROFILE = "backToCastProfile"
let SEGUE_TO_ADD_CAST_PROFILE = "AddCastProfileIdentifier"
let SEGUE_TO_ADD_CARTOONS = "AddCartoons"
let SEGUE_TO_ADD_TOYS = "addToys"
let UNWIND_TO_CARTOON = "backToCartoon"
let UNWIND_TO_TOYS = "backToToys"
let EDIT_CARTOON_SEGUE_IDENTIFIER = "editCartoon"
let EDIT_TOY_SEGUE_IDENTIFIER = "editToys"

let COLLECTIONVIEW_CELL_IDENTIFIER = "photoCell"
let COLLECTIONVIEW_CELL_IDENTIFIER_TOYS = "photoCellForToys"




