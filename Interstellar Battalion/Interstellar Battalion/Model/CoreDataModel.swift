//
//  CoreDataHandler.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 09/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHandler: NSObject
{
    
    private class func getContext() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        return appDelegate.persistentContainer.viewContext
    }
    
    class func saveObject(plot : String, url: String) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: ENTITY_NAME, in: context)
        let manageObject = NSManagedObject(entity: entity!, insertInto: context)
        
        manageObject.setValue(plot, forKey: PLOT_KEY)
        manageObject.setValue(url, forKey: URL_KEY)
        
        do {
            try context.save()
            return true
        }catch {
            return false
        }
    }
    
    class func fetchObject() -> [Plot]? {
        let context = getContext()
        var plot:[Plot]? = nil
        do {
            plot = try context.fetch(Plot.fetchRequest())
            return plot
        }catch {
            return plot
        }
    }
    
    class func updateObject(plot: String, url: String, oldPlot: String) -> Bool{
        let context = getContext()
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY_NAME)
        fetchRequest.predicate = NSPredicate(format: formatStringForPredicate(oldPlot: oldPlot))
        do {
            let results = try context.fetch(fetchRequest)
            if results.count == 1 {
                let objectUpdate = results[0] as! NSManagedObject
                objectUpdate.setValue(plot, forKey: PLOT_KEY)
                objectUpdate.setValue(url, forKey: URL_KEY)
                
                do{
                    try context.save()
                    return true
                }catch {
                    return false
                }
            }
        } catch {
            return false
        }
        return false
    }

}
