//
//  ToysCollectionViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 15/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class ToysCollectionViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    fileprivate var dataSource = ToyService.instance.dataSource
    
    var toyName : String?
    var toyImage : String?
    var toyDesc : String?
    var toyId : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        setup()
    }
    
    func setup() {
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_LOADING)
        ToyService.instance.getToys() {[weak self] (success) in
            if success {
                self?.hideLoadIndicator()
                self?.reloadPage()
            }
        }
    }
    
    fileprivate func reloadPage(){
        self.dataSource = ToyService.instance.dataSource
        self.collectionView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: SEGUE_TO_ADD_CARTOONS, sender: nil)
    }
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        self.setup()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == EDIT_TOY_SEGUE_IDENTIFIER {
            let destinationNavigationController = segue.destination as! UINavigationController
            let vc = destinationNavigationController.topViewController as! AddToysViewController
            vc.image = toyImage
            vc.name = toyName
            vc.descriptionText = toyDesc
            vc.id = toyId
        }
    }

}

extension ToysCollectionViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_LOADING)
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: COLLECTIONVIEW_CELL_IDENTIFIER_TOYS, for: indexPath) as? PhotoCellForToys {
            cell.record = self.dataSource[indexPath.row]
            self.hideLoadIndicator()
            return cell
        }
        return PhotoCellForToys()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        toyImage = self.dataSource[indexPath.row].image
        toyName = self.dataSource[indexPath.row].toy_name
        toyDesc = self.dataSource[indexPath.row].description
        toyId = self.dataSource[indexPath.row].id
        performSegue(withIdentifier: EDIT_TOY_SEGUE_IDENTIFIER, sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // number of columns
        let numOfColumns : CGFloat = 2
        // space between cells
        let spaceBetweenCells: CGFloat = 10
        // padding around cells
        let padding : CGFloat = 40
        // calculation to display 2 column for every device sice
        let cellDimension = ((collectionView.bounds.width - padding) - (numOfColumns) * spaceBetweenCells) / numOfColumns
        
        return CGSize(width: cellDimension,  height: cellDimension)
    }
    
}



