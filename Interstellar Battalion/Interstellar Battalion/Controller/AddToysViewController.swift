//
//  AddToysViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 15/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class AddToysViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var toyImage: UIImageView!
    @IBOutlet weak var toyName: UITextField!
    @IBOutlet weak var selectImageButton: UIButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var saveButton: UIButton!
    
    var name : String?
    var image: String?
    var descriptionText : String?
    var id: Int?
    
    var isEdit: Bool = false
    var imageStr: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        _ = self.textFieldShouldReturn(toyName)
        
        if name != nil && image != nil && descriptionTextView != nil {
            setupForEdit()
            toyName.isEnabled = false
            descriptionTextView.isEditable = false
            selectImageButton.isEnabled = false
            saveButton.isEnabled = false
        }
        
    }
    
    func setupForEdit() {
        guard let toyNameForTitle = name else {
            self.title = VIEW_CONTROLLER_TITLE_FOR_EDIT_ON_TOYS
            return
        }
        self.title = createTitleForToy(toyNameForTitle: toyNameForTitle)
        
        guard let toyNameForEdit = name else {
            toyName.placeholder = TOY_NAME_TEXTFIELD_PLACEHOLDER
            return
        }
        toyName.text = toyNameForEdit
        
        guard let toyImageForEdit = image else {
            toyImage.image = UIImage(named: IMAGE_NAME_DEFAULT_CARTOON)
            return
        }
        if let dataDecoded : Data = Data(base64Encoded: toyImageForEdit, options: .ignoreUnknownCharacters) {
            toyImage.image = UIImage(data: dataDecoded)
        }
        
        if descriptionText != nil {
            descriptionTextView.text = descriptionText
        } else {
            descriptionTextView.text = CARTOON_DESCRIPTION_TEXT_NOT_FOUND
        }
    }

    @IBAction func editButtonPressed(_ sender: Any) {
        toyName.isEnabled = true
        descriptionTextView.isEditable = true
        selectImageButton.isEnabled = true
        saveButton.isEnabled = true
        isEdit = true
    }
    
    @IBAction func selectImageButtonPressed(_ sender: Any) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: ALERT_CONTROLLER_TITLE, message: ALERT_CONTROLLER_MESSAGE, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_CAMERA_TITLE, style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            } else {
                self.showAlert(title: CAMERA_FAILED_TITLE, message: CAMERA_FAILED_MESSAGE)
            }
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_GALLERY_TITLE, style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: ALERT_ACTIONSHEET_CANCEL_TITLE, style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func savedButtonPressed(_ sender: Any) {
        
        if isEdit {
            self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPDATING)
            ToyService.instance.editToys(id: id!, toyName: toyName.text!, description: descriptionTextView.text, image: imageStr!) {[weak self] (success) in
                
                self?.hideLoadIndicator()
                if success {
                    self?.showAlertWithOk(title: ALERT_FOR_TOY_SAVED_TITLE, message: ALERT_FOR_TOY_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                        self?.performSegue(withIdentifier: UNWIND_TO_TOYS, sender: nil)
                    })
                }
            }
        } else {
            if imageStr == "" && toyName.text == "" && descriptionTextView.text == "" {
                self.showAlert(title: ALERT_FOR_TOY_SAVING_FAILED_TITLE, message: ALERT_FOR_TOY_SAVING_FAILED_MESSAGE)
            } else {
                self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPLOADING)
                CartoonService.instance.createCartoon(cartoonName: toyName.text!, description: descriptionTextView.text, image: imageStr!) {[weak self] (success) in
                    
                    self?.hideLoadIndicator()
                    if success {
                        self?.showAlertWithOk(title: ALERT_FOR_TOY_SAVED_TITLE, message: ALERT_FOR_TOY_SAVED_MESSAGE, completionHandler: {[weak self] (success) in
                            self?.performSegue(withIdentifier: UNWIND_TO_TOYS, sender: nil)
                        })
                    }
                }
            }
        }
        
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        toyImage.image = image
        picker.dismiss(animated: true, completion: nil)
        
        let imageData:NSData = UIImageJPEGRepresentation(image, 0.50)! as NSData
        imageStr = imageData.base64EncodedString(options: .lineLength64Characters)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

