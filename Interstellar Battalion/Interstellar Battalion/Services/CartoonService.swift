//
//  CartoonService.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 13/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CartoonService {
    
    static let instance = CartoonService()
    
    fileprivate var cartoonRecords = [CartoonDataModel]()
    
    var dataSource : [CartoonDataModel] {
        get {
            return cartoonRecords
        }
    }
    
    func createCartoon(cartoonName: String, description: String, image: String, completion: @escaping CompletionHandler) {
        
        let body: [String: Any] = [
            "cartoonName": cartoonName,
            "description": description,
            "image" : image
        ]
        
        Alamofire.request(URL_ADD_CARTOONS, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
    }
    
    func getCartoons(completion: @escaping CompletionHandler) {
        Alamofire.request(URL_GET_CARTOONS, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            
            if response.result.error == nil {
                guard let response = response.result.value as? [String:Any] else {return}
                
                guard let data = response["data"] as? [[String:Any]] else {
                    completion(false)
                    return
                }
                self.cartoonRecords.removeAll()
                for record in data {
                    let cartoonRecordData = CartoonDataModel(dict: record)
                    self.cartoonRecords.append(cartoonRecordData)
                }
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func editCartoon(id : Int, cartoonName: String, description: String, image: String, completion: @escaping CompletionHandler) {
        
        let body: [String: Any] = [
            "cartoonName": cartoonName,
            "description": description,
            "image" : image
        ]
        
        Alamofire.request(editCartoonUrl(id: id), method: .put, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}
