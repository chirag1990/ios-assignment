//
//  CastCareerModel.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 11/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation

struct CastCareerModel {
    let id: Int!
    let cast_screen_id: Int!
    let movie_name: String!
    let release_date: String!
    let short_description: String!
    let updated_at: String!
    let created_at: String!
    
    init(dict:[String:Any]){
        self.id = dict["id"] as! Int
        self.cast_screen_id = dict["cast_screen_id"] as! Int
        self.movie_name = dict["movie_name"] as! String
        self.release_date = dict["release_date"] as! String
        self.short_description = dict["short_description"] as! String
        self.created_at = dict["created_at"] as! String
        self.updated_at = dict["updated_at"] as! String
    }
}
