//
//  HomeViewController.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 08/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit
import CoreData
import WebKit

class HomeViewController: UIViewController {

    @IBOutlet weak var plotTextView: UITextView!
    @IBOutlet weak var movieTrailerWebView: WKWebView!
    
    var trailerUrl : String = ""
    var plot:[Plot]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    func setupUI() {
        self.showLoadIndicator(title: LOAD_INDICATOR_FOR_UPDATING)
        plot = CoreDataHandler.fetchObject()!
        if plot?.count == 0 {
            plotTextView.text = MOVIE_PLOT
            trailerUrl = MOVIE_TRAILER_URL
            
        } else {
            for plotText in plot! {
                plotTextView.text = plotText.plot
                trailerUrl = plotText.url!
            }
        }
        let url = URL(string: trailerUrl)
        movieTrailerWebView.load(URLRequest(url: url!))
        self.hideLoadIndicator()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        setupUI()
    }

}
