//
//  CastCell.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 09/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class CastCell: UITableViewCell {
    
    @IBOutlet fileprivate weak var lblDescription: UILabel!
    @IBOutlet fileprivate weak var lblName: UILabel!
    @IBOutlet fileprivate weak var imgCast: UIImageView!
    

    var record:CastData!{
        didSet{
            self.lblName.text = record.name
            self.lblDescription.text = record.des

            if let dataDecoded : Data = Data(base64Encoded: record.image, options: .ignoreUnknownCharacters) {
                imgCast.image = UIImage(data: dataDecoded)
            }
        }
    }
}

extension CastCell {
    struct CellConstant{
        static let Identifier = CAST_CELL_IDENTIFIER
        static let Height:CGFloat = 120.0
    }
    
    static func registerClass(tableView:UITableView){
        tableView.register(CastCell.self, forCellReuseIdentifier: CellConstant.Identifier)
    }
    
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: CellConstant.Identifier, bundle: nil), forCellReuseIdentifier: CellConstant.Identifier)
    }
}
