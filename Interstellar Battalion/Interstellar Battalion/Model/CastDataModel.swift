//
//  ModelStruct.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 09/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//


import Foundation

struct CastData
{
    let id:Int!
    let image:String!
    let name:String!
    let des:String!
    let createdAt:String!
    let updatedAt:String!
    
    init(dict:[String:Any]){
        self.id = dict["id"] as! Int
        self.image = dict["image"] as! String
        self.name = dict["name"] as! String
        self.des = dict["description"] as! String
        self.createdAt = dict["created_at"] as! String
        self.updatedAt = dict["updated_at"] as! String
    }
}
