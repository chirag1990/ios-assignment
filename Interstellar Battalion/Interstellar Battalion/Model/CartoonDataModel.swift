//
//  CartoonDataModel.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 14/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation

struct CartoonDataModel {
    let id : Int!
    let image : String!
    let cartoon_name : String!
    let description : String!
    let updated_at: String!
    let created_at: String!
    
    init(dict:[String:Any]){
        self.id = dict["id"] as! Int
        self.image = dict["image"] as! String
        self.cartoon_name = dict["cartoon_name"] as! String
        self.description = dict["description"] as! String
        self.updated_at = dict["updated_at"] as! String
        self.created_at = dict["created_at"] as! String
    }
}
