//
//  ViewController.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 05/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        _ = self.textFieldShouldReturn(username)
        _ = self.textFieldShouldReturn(password)
    }
    
    @IBAction func SignInButtonPressed(_ sender: Any) {
     
        if username.text == "" || password.text == "" {
            self.showAlert(title: USER_LOGIN_FAILED_TITLE, message: USER_LOGIN_FAILED_MESSAGE)
        } else {
            AuthService.instance.loginUser(email: username.text ?? "", password: password.text ?? "") { (success) in
                if success {
                    if AuthService.instance.isLoggedIn {
                        self.performSegue(withIdentifier: SEGUE_TO_HOME_FROM_LOGIN, sender: nil)
                    }
                } else {
                    self.showAlert(title: USER_LOGIN_FAILED_TITLE, message: USER_LOGIN_FAILED_MESSAGE)
                }
            }
        }
    }    
}


