//
//  UIViewController+MBProgressHud.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 09/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController
{
    func showLoadIndicator(title text:String) {
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        hud.label.text = text
    }
    
    func hideLoadIndicator(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func showAlert(title titleText:String, message messageText:String)
    {
        let alert = UIAlertController (title:titleText, message: messageText, preferredStyle: .alert)
        let okAction = UIAlertAction (title: "OK", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    
    func showAlertWithOk(title alertTitle:String, message alertMessage:String, completionHandler:@escaping AlertCompletionHandler )
    {
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            completionHandler(true)
        }
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
}
