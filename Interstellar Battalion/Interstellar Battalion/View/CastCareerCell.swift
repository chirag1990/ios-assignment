//
//  CastCareerCell.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 11/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import UIKit

class CastCareerCell: UITableViewCell
{
    //MARK:- IBOutlet
    @IBOutlet fileprivate weak var MovieNameLabel: UILabel!
    @IBOutlet fileprivate weak var MovieYearLabel: UILabel!
    @IBOutlet fileprivate weak var MovieDescLabel: UILabel!
    
    //MARK:- Public Vars
    var record:CastCareerModel!{
        didSet{
            self.MovieNameLabel.text = record.movie_name
            self.MovieYearLabel.text = record.release_date
            self.MovieDescLabel.text = record.short_description
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        MovieDescLabel.sizeToFit()
        MovieDescLabel.numberOfLines = 3
    }
}

//MARK:- Extension
extension CastCareerCell {
    struct CareerCellConstant{
        static let Identifier = CAST_CAREER_CELL_IDENTIFIER
        static let Height:CGFloat = 120.0
    }
    
    static func registerClass(tableView:UITableView){
        tableView.register(CastCareerCell.self, forCellReuseIdentifier: CareerCellConstant.Identifier)
    }
    
    static func registerNib(tableView:UITableView){
        tableView.register(UINib(nibName: CareerCellConstant.Identifier, bundle: nil), forCellReuseIdentifier: CareerCellConstant.Identifier)
    }
}
