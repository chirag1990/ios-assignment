//
//  CastService.swift
//  Interstellar Battalion
//
//  Created by Chirag Tailor on 09/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

//MARK:- Class
class CastService
{
    
    static let instance = CastService()
    
    //MARK:- Public Var
    var datsSource:[CastData]{
        get{
            return castRecords
        }
    }
    
    //MARK:- Private Var
    fileprivate var castRecords = [CastData]()
    
    func saveCast(image: String, name : String, description: String, completion: @escaping CompletionHandler)
    {
        let body: [String: Any] = [
            "image": image,
            "name": name,
            "description": description
        ]
        
        Alamofire.request(URL_ADD_CAST, method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func getCast(completion: @escaping CompletionHandler)
    {
        Alamofire.request(URL_GET_CAST, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            
            if response.result.error == nil
            {
                //guard let data = response.data else {return}
                guard let response = response.result.value as? [String:Any] else {return}
                
                guard let data = response["data"] as? [[String:Any]] else {
                    completion(false)
                    return
                }
                self.castRecords.removeAll()
                for record in data{
                    let castData = CastData(dict: record)
                    self.castRecords.append(castData)
                }
                
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
            
        }
    }
    func deleteCast(id: Int, completion: @escaping CompletionHandler) {
        Alamofire.request(deleteCastProfile(id: id), method: .delete, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func editCast(id: Int, image: String, name: String, description: String, completion: @escaping CompletionHandler) {
        
        let body: [String: Any] = [
            "image": image,
            "name": name,
            "description": description
        ]
        
        Alamofire.request(editCastProfile(id: id), method: .put, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
}


















