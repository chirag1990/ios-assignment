//
//  CastCareerService.swift
//  Interstellar Battalion
//
//  Created by Tailor, Chirag on 11/01/2018.
//  Copyright © 2018 TCTApps. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CastCareerService {
    
    static let instance = CastCareerService()
    
    
    var dataSource:[CastCareerModel]{
        get{
            return castCareerRecords
        }
    }
    
    fileprivate var castCareerRecords = [CastCareerModel]()
    
    func createCastCareer(cast_id: Int, movieName: String, releaseDate: String, shortDescription: String, completion: @escaping CompletionHandler) {
        let body: [String: Any] = [
            "movieName": movieName,
            "releaseDate": releaseDate,
            "shortDescription": shortDescription
        ]
        
        Alamofire.request(createCastCareerUrl(cast_id: cast_id), method: .post, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
    }
    
    func getCastCareer(cast_id: Int, completion: @escaping CompletionHandler) {
        Alamofire.request(getCastCareerUrl(cast_id: cast_id), method: .get, parameters: nil, encoding: JSONEncoding.default, headers: HEADER).responseJSON { (response) in
            
            if response.result.error == nil
            {
                //guard let data = response.data else {return}
                guard let response = response.result.value as? [String:Any] else {return}
                
                guard let data = response["data"] as? [[String:Any]] else {
                    completion(false)
                    return
                }
                self.castCareerRecords.removeAll()
                for record in data{
                    let castCareerData = CastCareerModel(dict: record)
                    self.castCareerRecords.append(castCareerData)
                }
                
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
            
        }
    }
    
    func editCareer(id: Int, career_id: Int, releaseDate: String, name: String, description: String, completion: @escaping CompletionHandler) {
        
        let body: [String: Any] = [
            "releaseDate": releaseDate,
            "movieName": name,
            "shortDescription": description
        ]
        
        Alamofire.request(editCastCareer(id: id, career_id: career_id), method: .put, parameters: body, encoding: JSONEncoding.default, headers: HEADER).responseString { (response) in
            if response.result.error == nil {
                completion(true)
            } else {
                completion(false)
                debugPrint(response.result.error as Any)
            }
        }
        
    }
    
}
